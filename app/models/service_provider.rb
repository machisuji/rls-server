class ServiceProvider < ActiveRecord::Base
  attr_accessible :name, :kind, :phone_number

  has_many :providers
end
