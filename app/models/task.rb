class Task < ActiveRecord::Base
  attr_accessible :customer_name, :phone_number, :src_address,
    :dst_address, :pickup_at, :created_at, :updated_at,
    :push_notification_token, :status, :eta, :estimated_price,
    :message, :number_of_passengers

  has_many :providers
  has_many :service_providers, :through => :providers

  module Status
    Rejected = "rejected"
    InProgress = "in-progress"
    Completed = "completed"

    def values
      [Rejected, InProgress, Completed]
    end
  end
end
