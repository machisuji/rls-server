class TasksController < ApplicationController

  def index
    respond_to do |format|
      format.html do
        @tasks = Task.all.select { |task| task.status != Task::Status::Completed }
      end
      format.json do
        render :json => Task.all.to_json(:include => :service_providers)
      end
    end
  end

  def show
  end

  def edit
    @task = Task.find params[:id]

    if !@task.nil? && @task.status == Task::Status::Completed
      redirect_to :action => "thanks"
    end
  end

  def update
    task = Task.find params[:id]
    task.update_attributes params[:task]

    destination = [task.push_notification_token]

    Rails.logger.info "Updated task: #{task.inspect}"

    if !task.push_notification_token.nil? && (params[:status] == "Complete" || params[:status] == "Amend")
      if params[:status] == "Complete"
        task.status = Task::Status::Completed
        task.save!
      end

      response = GCM.send_notification destination, task.attributes, :collapse_key => "task_update", :time_to_live => 60

      task.gcm_response = response.first
      task.save!
      Rails.logger.info(
        "Notified mobile device of update for Task #{task.id}, token: #{task.push_notification_token}")
    end

    respond_to do |format|
      format.all do
        redirect_to :action => (params[:status] == "Reject" ? "index" : "thanks")
      end
      format.json do
        render :json => {:message => "Updated task"}
      end
    end
  end

  def thanks

  end

  def create
    task = Task.create params[:task]

    task.service_providers << ServiceProvider.all

    Rails.logger.info "Created task: #{task.inspect}"

    create_hit(task)

    respond_to do |format|
      format.all do
        render :text => "created task", :status => 201, :location => url_for(task)
      end
      format.json do
        render :json => {:message => "Enqueued Task"}.to_json, :status => 201
      end
    end
  end

  def new
    
  end

  def destroy
    task = Task.find_by_id params[:id]

    unless task.nil?
      task.delete

      render :text => "", :status => 204
    else
      render :text => "no task found", :status => 404
    end
  end

  protected

  def create_hit (task)
    require 'rturk'

    aws = YAML.load(File.open(File.join(File.dirname(__FILE__), '../../config/mturk.yml')))
    RTurk::setup(aws['AWSAccessKeyId'], aws['AWSAccessKey'], :sandbox => false)

    hit = RTurk::Hit.create(:title => "Enter some text and click 'Complete'") do |hit|
      hit.description = "Enter a random message (a joke if you like) and click \"Complete\". Help us test our new app!"
      hit.reward = 0.05
      hit.assignments = 1
      hit.question(url_for :controller => "tasks", :action => "edit", :id => task.id)
      hit.duration = 300
    end

    Rails.logger.info "Created HIT #{hit.hit_id} at #{hit.url}"
  end

  def self.task_queue
    get_sqs.queues.named("rls_tasks")
  end

  def self.get_sqs
    AWS::SQS.new :access_key_id => "AKIAIYZWMCF7HQZKRSDQ",
      :secret_access_key => "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",
      :sqs_endpoint => "sqs.eu-west-1.amazonaws.com"
  end
end
