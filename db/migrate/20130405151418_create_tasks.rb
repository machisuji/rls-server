class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :src_address
      t.text :dst_address

      t.datetime :pickup_at
      t.string :customer_name
      t.string :phone_number

      t.timestamps
    end
  end
end
