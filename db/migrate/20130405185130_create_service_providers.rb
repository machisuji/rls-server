class CreateServiceProviders < ActiveRecord::Migration
  def change
    create_table :service_providers do |t|
      t.string :name
      t.string :kind
      t.string :phone_number

      t.timestamps
    end
  end
end
