class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.column :service_provider_id, :integer
      t.column :task_id, :integer
    end
  end
end
