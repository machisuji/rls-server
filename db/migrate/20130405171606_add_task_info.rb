class AddTaskInfo < ActiveRecord::Migration
  def change
    add_column :tasks, :status, :string
    add_column :tasks, :eta, :string
    add_column :tasks, :estimated_price, :string
    add_column :tasks, :message, :text
  end
end
