class Seed < ActiveRecord::Migration
  def up
    ServiceProvider.create :name => "City Cabs", :kind => "cab", :phone_number => "+441312281211"
    ServiceProvider.create :name => "ComCab", :kind => "cab", :phone_number => "+441312728000"
    ServiceProvider.create :name => "Central Taxis", :kind => "cab", :phone_number => "+441312292468"
  end

  def down
    ServiceProvider.find_by_name("Edinburgh Cabs").delete
    ServiceProvider.find_by_name("Markus").delete
  end
end
