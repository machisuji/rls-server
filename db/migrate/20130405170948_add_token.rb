class AddToken < ActiveRecord::Migration
  def change
    add_column :tasks, :push_notification_token, :string
  end
end
